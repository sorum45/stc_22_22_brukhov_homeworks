import java.util.Arrays;
import java.util.Random;

public class Homework_04 {
    public static void main(String[] args) {
        Random random = new Random();
        int[] array = new int[10];
        for (int i = 0; i < 10; i++){
            array[i] = random.nextInt(100);
        }
        System.out.println("Массив случайных чисел");
        System.out.println(Arrays.toString(array));

        int [] counts = new int[10];

        int localMin = 0;
        for (int i = 0; i < array.length; ++i) {
            if (i == array.length - 1) {
                if (array[i - 1] > array[i]) {
                    counts[i] = array[i];
                    localMin += 1;
                    break;
                }
            }
            else if (i == 0) {
                if (array[i] < array[i + 1]) {
                    counts[i] = array[i];
                    localMin += 1;
                }
            }
            else if (array[i] < array[i - 1] && array[i] < array[i + 1]) {
                counts[i] = array[i];
                localMin += 1;
            }

        }
        System.out.println("Локальные минимумы в массиве");
        System.out.println(Arrays.toString(counts));
        System.out.print("Всего локальных минимумов в массиве: " + localMin);
    }

}
